<?php
    function splitStudentsMarksIds($fileName){
        if($handle = fopen($fileName, 'r')){
            $data = array();
            while (!feof($handle)){
                array_push($data, fgets($handle));
            }
            foreach ($data as $key => $value) {
                $temp = explode(" ", $value);
                $student_marks[$temp[0]] = $temp[1];
            }
            return $student_marks;
            fclose($handle);
        }
        else{
            echo "File not found";
        }
    }
    function getStudentNumber($studentMarks){
        $count = 0;
        foreach ($studentMarks as $value){
            if($value >= 80){
                $count++;
            }
        }
        return $count;
    }
    $studentMarks = splitStudentsMarksIds('studentresult.txt');
    echo "Average score: ".(array_sum($studentMarks)/sizeof($studentMarks))."<br>";
    echo "Students scored greater or equal to 80: ".getStudentNumber($studentMarks)."<br><br><hr><br><br>";
?>

<form action="result_processing.php" method="GET">
    <label for='studentID'>Enter Student ID: </label>
    <input type='text' name='studentID'>
    <input type='submit' value='Show Grade'>
</form>